FROM fedora:33
ENV PHP8_VERSION=8.0.1
ENV CADDY_VERSION=2.3.0

# install extra repos
RUN dnf install -y 'dnf-command(config-manager)' && dnf install -y 'dnf-command(copr)' && dnf install -y https://rpms.remirepo.net/fedora/remi-release-33.rpm && dnf config-manager --set-enabled remi

# install PHP
RUN dnf module reset php && dnf module install -y php:remi-8.0 && dnf update -y
RUN dnf install -y php-$PHP8_VERSION

# install PHP extension
RUN dnf install -y php-curl php-iconv php-phar php-intl php-pdo php-pdo_pgsql php-pgsql php-sodium php-xml php-pecl-redis php-openssl php-ctype php-tokenizer

# install composer
RUN dnf install -y composer

# install Caddy
RUN dnf copr enable -y @caddy/caddy && dnf install -y caddy-$CADDY_VERSION

# add config
COPY Caddyfile /etc/caddy
COPY www.conf /etc/php-fpm.d/www.conf
RUN mkdir -p /var/run/php-fpm/

# create app environment
RUN mkdir -p /srv/app
COPY . /srv/app
RUN chown -R apache /srv

WORKDIR /srv/app

CMD php-fpm --daemonize && caddy run